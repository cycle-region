![][image]

## About

`cycle-region` provides a region ring you can interactively go through.

## Usage

First, enable region tracking:

    (cycle-region-mode)

Then bind the preview to a suitable key:

    (global-set-key (kbd "C-c r") 'cycle-region-preview)

You can now use `C-c r` to open the region ring preview.  The
following keybindings are now active:

| Key bind | Function                          |
|----------+-----------------------------------|
| p        | Switch to previous region         |
| n        | Switch to next region             |
| RET      | Active currently previewed region |
| q        | Quit preview                      |

Any other key will quit the preview as well.

^ Feels excessive...

## Evil integration

    (add-hook 'cycle-region-pre-preview-hook 'evil-emacs-state)
    (add-hook 'cycle-region-post-preview-hook 'evil-normal-state)

    (define-key evil-normal-state-map (kbd "g V") 'cycle-region-preview)

[image]: cycle.gif
